alias ci_prepare='CI_JOB_STAGE=prepare $CI_PROJECT_DIR/build.sh'
alias ci_ensure='CI_JOB_STAGE=ensure $CI_PROJECT_DIR/build.sh'
alias ci_test='CI_JOB_STAGE=test $CI_PROJECT_DIR/build.sh'
alias ci_build='CI_JOB_STAGE=build $CI_PROJECT_DIR/build.sh'

set -o vi