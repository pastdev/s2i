FROM golang:1.10

ENV CI_COMMIT_TAG 0.0.0
ENV CI_PROJECT_NAME s2i
ENV CI_PROJECT_NAMESPACE pastdev
ENV CI_PROJECT_PATH ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
ENV CI_PROJECT_DIR /builds/${CI_PROJECT_PATH}
ENV DEP_VERSION 0.4.1
ENV GOPATH ${CI_PROJECT_DIR}/.go
ENV ARTIFACTS_DIR ${CI_PROJECT_DIR}/.artifacts
ENV GO_PROJECT_NAMESPACE_DIR ${GOPATH}/src/gitlab.com/${CI_PROJECT_NAMESPACE}
ENV GO_PROJECT_DIR ${GO_PROJECT_NAMESPACE_DIR}/${CI_PROJECT_NAME}
ENV PLATFORMS "windows linux darwin"

ENV PATH ${GOPATH}/bin:${PATH}

COPY . ${CI_PROJECT_DIR} 

RUN set -e; \
    echo '. $CI_PROJECT_DIR/.dockerfile/.bashrc' >> ~/.bashrc; \
    chmod +x ${CI_PROJECT_DIR}/build.sh;

WORKDIR ${CI_PROJECT_DIR}
