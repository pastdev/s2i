# S2I Utilities
This project is comprised of many `go` utilities that have been found useful
in `s2i` builder images.

## CI
This project is configured to use gitlab-ci for builds.  If succesful
cross-compiled artifacts will be generated and written to the artifact folder
of the project release.

### build.sh
The `build.sh` script included in thies project is used by the `.gitlab-ci.yml`
for all of the _scripted_ work.  Use the included `Dockerfile` for testing the
build process.

### Dockerfile
The `Dockerfile` included in this project is intended to help test the CI code.
You can use it to run the `build.sh` stages.

#### Building
Build the image using this command:
```
docker build . --no-cache --tag pastdev/s2i:latest
```

#### Running
Run the image using this command:
```
docker run -it --rm pastdev/s2i
```
Inside of the image, there have been aliases created for each job stage.  They
are of the form `ci_<stage>` (ie: `ci_prepare`).  To mimic the build you may
do the following:
```
ci_prepare
ci_ensure exportjavaproxy
ci_test exportjavaproxy
CI_COMMIT_TAG=1.2.3 ci_build exportjavaproxy
http_proxy=http://proxy.pastdev.com:80 .artifacts/exportjavaproxy-linux
```