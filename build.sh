#!/bin/bash

set -e

# This script is intended to be run inside of the golang:1.10
# docker image as part of the CI build.  Assumptions are made to
# that effect...

# expects project as argument (ex: exportjavaproxy)
PROJECT=$1

if [ -z $CI_COMMIT_TAG ]; then
  CI_COMMIT_TAG=0.0.0
fi

# a little validation
if [ -z $ARTIFACTS_DIR ]; then echo "missing ARTIFACTS_DIR"; exit 1; fi
if [ -z $CI_COMMIT_TAG ]; then echo "missing CI_COMMIT_TAG"; exit 1; fi
if [ -z $CI_JOB_STAGE ]; then echo "missing CI_JOB_STAGE"; exit 1; fi
if [ -z $CI_PROJECT_PATH ]; then echo "missing CI_PROJECT_PATH"; exit 1; fi
if [ -z $DEP_VERSION ]; then echo "missing DEP_VERSION"; exit 1; fi
if [ -z $GOPATH ]; then echo "missing GOPATH"; exit 1; fi
if [ -z $GO_PROJECT_DIR ]; then echo "missing GO_PROJECT_DIR"; exit 1; fi
if [ -z $GO_PROJECT_NAMESPACE_DIR ]; then echo "missing GO_PROJECT_NAMESPACE_DIR"; exit 1; fi
if [ -z "$PLATFORMS" ]; then echo "missing PLATFORMS"; exit 1; fi

if [[ $CI_JOB_STAGE = "prepare" ]] || [ ! -d ${GO_PROJECT_DIR} ]; then
  echo "prepare"
  mkdir -p ${GOPATH}/src ${GOPATH}/bin
  curl -L -s https://github.com/golang/dep/releases/download/v${DEP_VERSION}/dep-linux-amd64 -o $GOPATH/bin/dep
  chmod +x ${GOPATH}/bin/dep
  mkdir -p ${GO_PROJECT_DIR}
  tar -c \
      --exclude ".go" \
      --exclude ".git*" \
      --exclude "Dockerfile" \
      --exclude ".dockerfile" \
      --exclude "README.md" \
      . | tar -x -C $GO_PROJECT_DIR
  echo "prepare complete"
  if [[ $CI_JOB_STAGE = "prepare" ]]; then DONE=1; fi
fi

if [ ! -z ${PROJECT} ]; then
  pushd ${GO_PROJECT_DIR}/${PROJECT}

  if [ ! $DONE ]; then
    if [[ $CI_JOB_STAGE = "ensure" ]] || [ ! -d vendor ]; then
      echo "ensure"
      pwd
      dep ensure
      echo "ensure complete"
      if [[ $CI_JOB_STAGE = "ensure" ]]; then DONE=1; fi
    fi
  fi

  if [ ! $DONE ]; then
    if [[ $CI_JOB_STAGE = "test" ]]; then
      echo "test"
      go test .
      echo "test complete"
      if [[ $CI_JOB_STAGE = "test" ]]; then DONE=1; fi
    fi
  fi

  if [ ! $DONE ]; then
    if [[ $CI_JOB_STAGE = "build" ]]; then
      echo "build"
      for PLATFORM in ${PLATFORMS}; do
        EXTENSION=""
        if [[ ${PLATFORM} = 'windows' ]]; then
          EXTENSION=".exe"
        fi
        export GOOS=${PLATFORM}
        export GOARCH=amd64
        export CGO_ENABLED=0

        OUT_FILE="${ARTIFACTS_DIR}/${PROJECT}-${PLATFORM}${EXTENSION}"
        echo "Creating ${OUT_FILE} (${CI_COMMIT_TAG})"
        go build \
          -ldflags "-X gitlab.com/${CI_PROJECT_PATH}/${PROJECT}/main.version=${CI_COMMIT_TAG}" \
          -o ${OUT_FILE}
      done
      ls -lrt ${ARTIFACTS_DIR}
      echo "build complete"
      if [[ $CI_JOB_STAGE = "build" ]]; then DONE=1; fi
    fi
  fi

  popd
fi