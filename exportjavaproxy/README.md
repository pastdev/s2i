# exportjavaproxy
The base s2i container for java gathers its maven proxy configuration from the
following environment variables:

* `HTTP_PROXY_HOST`
* `HTTP_PROXY_PORT`
* `HTTPS_PROXY_HOST`
* `HTTPS_PROXY_PORT`
* `NON_PROXY_HOSTS`

Since proxy information is typically supplied as `http_proxy`, `https_proxy`,
and `no_proxy` to the container already, this command will convert these 
environment variables into those expected by the maven configuration.

# Usage
Add the following to any of your s2i bash scripts:
```bash
source <(exportjavaproxy)
...