package main

import (
	"bytes"
	"fmt"
	"net/url"
	"os"
	"sort"
	"strings"
)

func parseNoProxy(noProxy string) []string {
	if noProxy == "" {
		return []string{}
	}
	noProxyHosts := strings.Split(noProxy, ",")
	for i, value := range noProxyHosts {
		if strings.HasPrefix(value, ".") {
			noProxyHosts[i] = "*" + value
		}
	}
	return noProxyHosts
}

func parseProxy(proxy string) (string, string) {
	if proxy == "" {
		return "", ""
	}

	var url, err = url.Parse(proxy)
	if err != nil {
		return "", ""
	}

	var port = url.Port()
	if port == "" {
		if url.Scheme == "https" {
			port = "443"
		} else {
			port = "80"
		}
	}

	return url.Hostname(), port
}

func toScript(exports map[string]string) string {
	var script bytes.Buffer
	var keys bytes.Buffer

	var sorted []string
	for k := range exports {
		sorted = append(sorted, k)
	}
	sort.Strings(sorted)

	for _, key := range sorted {
		value := exports[key]
		if value != "" {
			script.WriteString(fmt.Sprintf("%s=\"%s\"\n", key, value))
			keys.WriteString(fmt.Sprintf(" %s", key))
		}
	}
	if keys.Len() > 0 {
		script.WriteString(fmt.Sprintf("export%s\n", keys.String()))
	}

	return script.String()
}

func main() {
	var exports = make(map[string]string)
	exports["HTTP_PROXY_HOST"], exports["HTTP_PROXY_PORT"] = parseProxy(os.Getenv("http_proxy"))
	exports["HTTPS_PROXY_HOST"], exports["HTTPS_PROXY_PORT"] = parseProxy(os.Getenv("https_proxy"))
	exports["NON_PROXY_HOSTS"] = strings.Join(parseNoProxy(os.Getenv("no_proxy")), "|")

	fmt.Print(toScript(exports))
}
