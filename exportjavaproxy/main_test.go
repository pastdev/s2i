package main

import (
	"os"
	"reflect"
	"regexp"
	"testing"
)

func TestParseProxy(t *testing.T) {
	hostname, port := parseProxy("")
	if !(hostname == "" && port == "") {
		t.Error("Empty string parse proxy failed")
	}

	hostname, port = parseProxy("not:a:scheme:////foobar!@#$%^&")
	if !(hostname == "" && port == "") {
		t.Error("Invalid url parse proxy failed")
	}

	hostname, port = parseProxy("http://pastdev.com:80")
	if !(hostname == "pastdev.com" && port == "80") {
		t.Error("Basic parse proxy failed")
	}

	hostname, port = parseProxy("http://pastdev.com")
	if !(hostname == "pastdev.com" && port == "80") {
		t.Error("Parse proxy hostname only failed")
	}

	hostname, port = parseProxy("https://pastdev.com")
	if !(hostname == "pastdev.com" && port == "443") {
		t.Error("Parse proxy https hostname only failed")
	}
}

func TestParseNoProxy(t *testing.T) {
	noProxy := parseNoProxy("")
	if len(noProxy) != 0 {
		t.Errorf("Parse no proxy empty string failed: %v", noProxy)
	}

	noProxy = parseNoProxy("localhost,127.0.0.1,pastdev.com")
	if !reflect.DeepEqual(noProxy, []string{"localhost", "127.0.0.1", "pastdev.com"}) {
		t.Error("Parse no proxy three values failed")
	}

	noProxy = parseNoProxy(".foo.bar,localhost,.hip.hop,pastdev.com")
	if !reflect.DeepEqual(noProxy, []string{"*.foo.bar", "localhost", "*.hip.hop", "pastdev.com"}) {
		t.Error("Parse no proxy domain suffix failed")
	}
}

func TestToScript(t *testing.T) {
	script := toScript(map[string]string{"FOO": "bar"})
	expected := "FOO=\"bar\"\nexport FOO\n"
	if script != expected {
		t.Errorf("One var script failed [%v] != [%v]", script, expected)
	}

	script = toScript(map[string]string{"FOO": "bar", "BAZ": "qux"})
	expected = "(?m)^FOO=\"bar\"$"
	if matched, err := regexp.MatchString(expected, script); err != nil || !matched {
		t.Errorf("Two var script failed [%v] != [%v]", script, expected)
	}
	expected = "(?m)^BAZ=\"qux\"$"
	if matched, err := regexp.MatchString(expected, script); err != nil || !matched {
		t.Errorf("Two var script failed [%v] != [%v]", script, expected)
	}
	expected = "(?m)^export( (FOO|BAZ)){2}$"
	if matched, err := regexp.MatchString(expected, script); err != nil || !matched {
		t.Errorf("Two var script failed [%v] != [%v]", script, expected)
	}
}

func Example_testParseAll() {
	os.Setenv("http_proxy", "http://pastdev.com:80")
	os.Setenv("https_proxy", "http://patdev.com:80")
	os.Setenv("no_proxy", ".foo.bar,localhost,.hip.hop,pastdev.com")
	main()
	// Output:
	// HTTPS_PROXY_HOST="patdev.com"
	// HTTPS_PROXY_PORT="80"
	// HTTP_PROXY_HOST="pastdev.com"
	// HTTP_PROXY_PORT="80"
	// NON_PROXY_HOSTS="*.foo.bar|localhost|*.hip.hop|pastdev.com"
	// export HTTPS_PROXY_HOST HTTPS_PROXY_PORT HTTP_PROXY_HOST HTTP_PROXY_PORT NON_PROXY_HOSTS
}

func Example_testParseHttpOnly() {
	os.Setenv("http_proxy", "http://pastdev.com:80")
	os.Unsetenv("https_proxy")
	os.Setenv("no_proxy", ".foo.bar,localhost,.hip.hop,pastdev.com")
	main()
	// Output:
	// HTTP_PROXY_HOST="pastdev.com"
	// HTTP_PROXY_PORT="80"
	// NON_PROXY_HOSTS="*.foo.bar|localhost|*.hip.hop|pastdev.com"
	// export HTTP_PROXY_HOST HTTP_PROXY_PORT NON_PROXY_HOSTS
}
